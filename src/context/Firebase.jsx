import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyABKHvVW2Onrl9BM-GZX0al_Ch8340g7gU",
  authDomain: "react-authentication-45bfe.firebaseapp.com",
  projectId: "react-authentication-45bfe",
  storageBucket: "react-authentication-45bfe.appspot.com",
  messagingSenderId: "917527166349",
  appId: "1:917527166349:web:eda67aefce7293c27e91c4",
  measurementId: "G-7SW28ZFFZD",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;
