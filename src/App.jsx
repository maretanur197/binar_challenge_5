import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { gapi } from "gapi-script";

import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import CatalogTv from "./pages/CatalogTv";
import ViewMovie from "./components/ViewMovie";
import Detail from "./pages/Detail";
import Search from "./pages/Search";
import Login from "./components/User/Login";
import Register from "./components/User/Register";
import { UserAuthContextProvider } from "./context/UserAuthContext";
import ProtectedRoute from "././components/User/ProtectedRoute";
import Logout from "./components/User/Logout";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <UserAuthContextProvider>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/ViewMovie" element={<ViewMovie />} />
            <Route path="/CatalogTv" element={<CatalogTv />} />
            <Route path="/:id" element={<Detail />} />
            <Route path="/search" element={<Search />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
          </Routes>
        </UserAuthContextProvider>
        <Footer />
      </BrowserRouter>
    </div>
  );
};
export default App;
