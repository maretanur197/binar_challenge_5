import React from "react";
import { useUserAuth } from "../../context/UserAuthContext";

const Logout = () => {
  const handleLogOut = async () => {
    try {
      await logOut();
    } catch (err) {
      console.log(err.message);
    }
  };
  return <div onClick={handleLogOut}>Logout</div>;
};

export default Logout;
