import React from "react";
import bg from "../../assets/imgLogin.jpg";
import { AiFillEye } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useUserAuth } from "../../context/UserAuthContext";

const Register = () => {
  const navigate = useNavigate();

  const clikLogin = (i) => {
    navigate(`/login`);
  };

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const { signUp } = useUserAuth();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");

    try {
      await signUp(email, password);
      navigate(`/login`);
    } catch (err) {
      setError(err.message);
    }
  };

  return (
    <section class="bg-gray-50 min-h-screen flex items-center justify-center p-5">
      <div class="bg-gray-100 flex rounded-2xl shadow-lg max-w-3xl p-8 items-center">
        <div class="md:w-1/2 px-8 md:px-16">
          <h2 class="font-bold text-2xl text-[#002D74] text-center pb-8">
            Register
          </h2>
          {error && <p>{error}</p>}

          <form action="" class="flex flex-col gap-4" onSubmit={handleSubmit}>
            <input
              class="p-2 rounded-xl border"
              type="email"
              name="email"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
            />

            <div class="relative">
              <input
                class="p-2 rounded-xl border w-full"
                type="password"
                name="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <AiFillEye class="text-gray-600 absolute top-1/2 right-3 -translate-y-1/2" />
            </div>

            <button class="bg-[#002D74] rounded-xl text-white py-2 hover:scale-105 duration-300">
              Register Now
            </button>
          </form>

          <div class="mt-10 text-xs flex justify-center items-center text-[#002D74]">
            <p>Have an account?</p>
            <button
              class="py-2 px-2 underline  hover:scale-110 duration-300"
              onClick={clikLogin}
            >
              LogIn
            </button>
          </div>
        </div>

        <div class="md:block hidden w-1/2">
          <img class="rounded-2xl" src={bg} />
        </div>
      </div>
    </section>
  );
};

export default Register;
