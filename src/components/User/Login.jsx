import React, { useEffect, useState } from "react";
import bg from "../../assets/imgLogin.jpg";
import { AiFillEye } from "react-icons/ai";
import { BsGithub } from "react-icons/bs";
import GitHubButton from "react-github-btn";
import { GoogleButton } from "react-google-button";
import { useNavigate } from "react-router-dom";
import { useUserAuth } from "../../context/UserAuthContext";

const Login = () => {
  const navigate = useNavigate();
  const clikRegister = (i) => {
    navigate(`/register`);
  };

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const { logIn, googleSignIn, githubSignIn } = useUserAuth();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");

    try {
      await logIn(email, password);
      navigate(`/home`);
    } catch (err) {
      setError(err.message);
    }
  };

  const handleGoogleSignIn = async (e) => {
    e.preventDefault();

    try {
      await googleSignIn();
      navigate(`/home`);
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleGithubSignIn = async (e) => {
    e.preventDefault();

    try {
      await githubSignIn();
      navigate(`/home`);
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <section class="bg-gray-50 min-h-screen flex items-center justify-center p-5">
      <div class="bg-gray-100 flex rounded-2xl shadow-lg max-w-3xl p-5 items-center">
        <div class="md:w-1/2 px-8 md:px-16">
          <h2 class="font-bold text-2xl text-[#002D74] text-center">Login</h2>

          {error && <p>{error}</p>}
          <form action="" class="flex flex-col gap-4" onClick={handleSubmit}>
            <input
              class="p-2 mt-8 rounded-xl border"
              type="email"
              name="email"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
            />

            <div class="relative">
              <input
                class="p-2 rounded-xl border w-full"
                type="password"
                name="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <AiFillEye class="text-gray-600 absolute top-1/2 right-3 -translate-y-1/2" />
            </div>

            <div class="justify-between text-xs text-[#002D74]">
              <a href="#">Forgot your password?</a>
            </div>

            <button class="bg-[#002D74] rounded-xl text-white py-2 hover:scale-105 duration-300">
              Login
            </button>
          </form>

          <div class="mt-6 grid grid-cols-3 items-center text-gray-400">
            <hr class="border-gray-400" />
            <p class="text-center text-sm">OR</p>
            <hr class="border-gray-400" />
          </div>

          <div className="pt-5 ">
            <GoogleButton onClick={handleGoogleSignIn} />
          </div>
          <div className="pt-5  ">
            <button
              onClick={handleGithubSignIn}
              className="flex gap-3 py-2 bg-[#4285F4] p-1 w-60"
            >
              <BsGithub className="bg-white w-10 h-8" />
              <p className="text-white text-center justify-center">
                {" "}
                Sign in with Github
              </p>
            </button>
          </div>

          <div class="mt-3 text-xs flex justify-center items-center text-[#002D74]">
            <p>Don't have an account?</p>
            <button
              class="py-2 px-2 underline  hover:scale-110 duration-300"
              onClick={clikRegister}
            >
              Register
            </button>
          </div>
        </div>

        <div class="md:block hidden w-1/2">
          <img class="rounded-2xl" src={bg} />
        </div>
      </div>
    </section>
  );
};

export default Login;
