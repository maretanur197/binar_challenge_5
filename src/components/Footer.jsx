import React from "react";
import logo from "../assets/logo.png";
import { Link, useLocation } from "react-router-dom";

import { BsFacebook, BsTwitter, BsInstagram } from "react-icons/bs";

const Footer = () => {
  const location = useLocation();

  const isLogin = location.pathname === "/login";
  const isRegister = location.pathname === "/register";

  return (
    <>
      {!isLogin && !isRegister && (
        <footer className="w-full pt-10 bg-black text-white">
          <div className=" p-6 flex flex-col justify-between py-10 mx-auto space-y-6 lg:flex-row lg:space-y-0">
            <div className="">
              <Link
                to="/"
                className="flex justify-center items-center lg:justify-start"
              >
                <div className="flex items-center justify-center w-12 h-12 rounded-full ">
                  <img src={logo} />
                </div>
                <span className="text-center text-2xl font-semibold hover:text-red-600">
                  MovieList
                </span>
              </Link>
            </div>
            <div className="grid grid-cols-2 text-sm gap-x-3 gap-y-8 lg:w-2/3 sm:grid-cols-4">
              <div className="space-y-3">
                <h3
                  className="tracking-wide uppercase font-semibold  hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-16 hover:before:opacity-100"
                >
                  About
                </h3>
                <ul className="space-y-1">
                  <li className="hover:text-red-600 ">
                    <Link to="/">Home</Link>
                  </li>

                  <li className="hover:text-red-600 ">
                    <Link to="/CatalogMovie">Movie</Link>
                  </li>

                  <li className="hover:text-red-600 ">
                    <Link to="/CatalogTv">Tv Series</Link>
                  </li>
                </ul>
              </div>
              <div className="space-y-3">
                <h3
                  className="tracking-wide uppercase font-semibold hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-16 hover:before:opacity-100"
                >
                  Company
                </h3>
                <ul className="space-y-1">
                  <li className="hover:text-red-600 ">FAQ</li>
                  <li className="hover:text-red-600 ">Terms of Service</li>
                </ul>
              </div>
              <div className="space-y-3">
                <h3
                  className="uppercase font-semibold hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-16 hover:before:opacity-100"
                >
                  Developers
                </h3>
                <ul className="space-y-1">
                  <li className="hover:text-red-600 ">TMDB</li>
                  <li className="hover:text-red-600 ">Taildwind</li>
                  <li className="hover:text-red-600 ">ReactJS</li>
                </ul>
              </div>
              <div className="space-y-3">
                <div
                  className="uppercase font-semibold hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-16 hover:before:opacity-100"
                >
                  Social media
                </div>
                <div className="flex justify-start space-x-3">
                  <BsFacebook className="w-5 h-5 fill-current" />
                  <BsTwitter className="w-5 h-5 fill-current" />
                  <BsInstagram className="w-5 h-5 fill-current" />
                </div>
              </div>
            </div>
          </div>
          <div className="h-12 p-3 w-full text-sm text-center bg-black text-white">
            © 2023 Copyright : MovieList
          </div>
        </footer>
      )}
    </>
  );
};

export default Footer;
