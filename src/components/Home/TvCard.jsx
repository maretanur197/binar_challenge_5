import React, { useEffect, useState } from "react";
import axios from "axios";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Autoplay, Navigation, Scrollbar, A11y } from "swiper";
import "swiper/swiper-bundle.min.css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Link, useNavigate } from "react-router-dom";

const TvCard = () => {
  const [movies, setMovies] = useState([]);
  const [slidesPerView, setSlidesPerView] = useState(7);

  useEffect(() => {
    axios
      .get(`https://api.themoviedb.org/3/tv/popular`, {
        params: {
          api_key: "24421cafe490cc00aeb89c9771051f13",
        },
      })
      .then((response) => {
        setMovies(response.data.results);
      });

    const handleResize = () => {
      const windowWidth = window.innerWidth;
      if (windowWidth >= 1280) {
        setSlidesPerView(4);
      } else if (windowWidth >= 700) {
        setSlidesPerView(4);
      } else {
        setSlidesPerView(2);
      }
    };
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <div className="p-5 lg:px-10 md:px-10 bg-black">
      <div className="grid grid-cols-2 lg:gap-96 text-white pt-6 pb-4">
        <h3 className="text-sm lg:text-2xl font-semibold uppercase ">
          <span
            className=" relative cursor-pointer transition-all 
            before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
            before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
          >
            Popular TV
          </span>
        </h3>
        <div className="flex text-red-600 text-xs lg:text-xl">
          <p className="pl-12 lg:pl-24 flex gap-2">
            <a href="/CatalogTv"> View More</a>
            <ion-icon name="arrow-forward-outline"></ion-icon>
          </p>
        </div>
      </div>

      <Swiper
        slidesPerView={slidesPerView}
        spaceBetween={50}
        loop={true}
        scrollbar={{
          draggable: true,
        }}
        Pagination={{
          clickable: true,
        }}
        direction="horizontal"
        autoplay={{
          delay: 8000,
          disableOnInteraction: false,
        }}
        navigation={{
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        }}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(Swiper) => console.log(Swiper)}
        modules={[Pagination, Autoplay, Navigation, Scrollbar, A11y]}
      >
        <div className="pt-10 items-center justify-center w-full">
          {movies.slice(0, 8).map((movie) => (
            <SwiperSlide key={movie.id}>
              <div className="w-full">
                <div className="mt-3 hover:scale-95 transition relative overflow-hidden">
                  <img
                    src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                    alt=""
                    className="w-full h-full rounded-md"
                    onClick={() => handleClickDetail(movie.id)}
                  />
                  <h2 className="text-white truncate capitalize text-xs w-52 pt-4">
                    {movie.name}
                  </h2>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </div>
        <button className="swiper-button-prev font-extrabold text-red-500 scale-50"></button>
        <button className="swiper-button-next font-extrabold text-red-500 scale-50"></button>
      </Swiper>
    </div>
  );
};

export default TvCard;
