import React from "react";
import MovieCard from "./Home/MovieCard";
import { Link } from "react-router-dom";

const PopularMovie = () => {
  return (
    <div className="p-5 lg:px-10 md:px-10 bg-black">
      <div className="grid grid-cols-2 lg:gap-96 text-white py-6">
        <h3 className="text-sm lg:text-2xl font-semibold uppercase ">
          <span
            className=" relative cursor-pointer transition-all 
          before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
          before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
          >
            Popular Movie
          </span>
        </h3>
        <div className="flex text-red-600 text-xs lg:text-xl">
          <p className="pl-12 lg:pl-24 flex gap-2">
            <Link to="/ViewMovie">View More</Link>

            <ion-icon name="arrow-forward-outline"></ion-icon>
          </p>
        </div>
      </div>
      <MovieCard />
    </div>
  );
};

export default PopularMovie;
