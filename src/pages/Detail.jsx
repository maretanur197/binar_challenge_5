import React, { useEffect, useState } from "react";
import axios from "axios";

import { AiFillStar } from "react-icons/ai";
import img from "../assets/up.jpg";
import { useParams } from "react-router-dom";
import MovieCard from "../components/Home/MovieCard";

const Detail = () => {
  const [movie, setMovie] = useState({});
  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`https://api.themoviedb.org/3/movie/${id}`, {
        params: {
          api_key: "24421cafe490cc00aeb89c9771051f13",
        },
      })
      .then((response) => {
        setMovie(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  return (
    <div className="w-full">
      <div className="md:pb-20">
        <img
          src={`https://image.tmdb.org/t/p/original/${movie.backdrop_path}`}
          alt="movie"
          className="w-full h-80 md:h-[80vh] lg:h-[100vh] object-cover opacity-50"
        />
        <div className="absolute top-20 md:top-36 lg:top-36 p-3 w-full h-full mx-auto lg:p-14">
          <div className=" grid grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-9 ">
            <div className="pt-2 lg:pt-2 pl-2 flex">
              <img
                src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                alt="movie"
                className="w-52 lg:w-80 md:w-80 rounded-xl "
              />
            </div>
            <div className="">
              <h1 className="text-2xl w-full text-white font-bold pt-5 flex md:text-5xl md:pt-10 lg:text-6xl">
                {movie.title}
              </h1>
              <div className=" flex py-2">
                <p className=" text-yellow-500 w-6 text-bases lg:pt-1">
                  <AiFillStar />
                </p>
                <p className=" text-yellow-500 text-base lg:pt-1">
                  {movie.vote_average}
                </p>
              </div>
              <p className="text-white text-[7px] w-36 md:w-72 lg:w-80 md:text-justify lg:text-base md:text-sm pb-3">
                {movie.overview}
              </p>
              <button className="flex flex-rows gap-2 items-center p-2 lg:p-5 bg-red-600 text-center rounded w-28 lg:w-36 h-8 lg:h-10 text-white text-xs lg:text-sm">
                <ion-icon name="play"></ion-icon>
                <p> Watch Trailer</p>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="text-white pt-20 p-8">
        <div>
          <h3 className="text-sm lg:text-2xl font-semibold uppercase py-8">
            <span
              className=" relative cursor-pointer transition-all 
          before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
          before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
            >
              Backdrop
            </span>
          </h3>
          <img
            src={`https://image.tmdb.org/t/p/original/${movie.backdrop_path}`}
            alt="movie"
            className="w-full h-full md:h-full lg:h-full"
          />
        </div>
        <div>
          <h3 className="text-sm lg:text-2xl font-semibold uppercase pb-3 pt-12">
            <span
              className=" relative cursor-pointer transition-all 
          before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
          before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
            >
              You May also like
            </span>
          </h3>
          <MovieCard />
        </div>
      </div>
    </div>
  );
};

export default Detail;
