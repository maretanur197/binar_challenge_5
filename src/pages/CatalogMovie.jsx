import React from "react";
import ViewMovie from "../components/ViewMovie";

const CatalogMovie = () => {
  return (
    <div>
      <ViewMovie />
    </div>
  );
};

export default CatalogMovie;
