// import axios from "axios";
// const NODE_ENV = process.env.NODE_ENV;

// const apiKey = process.env.REACT_APP_APIKEY;
// const baseUrl = process.env.REACT_APP_BASEURL;

// export const getMovieList = async () => {
//   const movie = await axios.get(
//     `${baseUrl}/movie/popular?page=1&api_key=${apiKey}`
//   );
//   return movie.data.results;
// };

// export const searchMovie = async (q) => {
//   const search = await axios.get(
//     `${baseUrl}/search/movie?query=${q}&page=1&api_key=${apiKey}`
//   );
//   return search.data;
// };

import axios from "axios";

export const getMovieList = async () => {
  const movie = await axios.get(
    `https://api.themoviedb.org/3/movie/popular?api_key=8625d30a47546513e5ec6c4b16b9d46a`
  );
  return;
};

export const searchMovie = async (q) => {
  const search = await axios.get(q);
  return;
};
