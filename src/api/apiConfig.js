// const apiConfig = {
//   baseUrl: "https://api.themoviedb.org/3/",
//   apiKey: "24421cafe490cc00aeb89c9771051f13",
//   originalImange: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
//   w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,
// };

// export default apiConfig;

import axios from "axios";

export default axios.create({
  baseUrl: "https://api.themoviedb.org/3/",
  headers: {
    Accept: "application/json",
  },
  params: {
    apiKey: "24421cafe490cc00aeb89c9771051f13",
  },
});
